﻿using UnityEngine;
using System.Collections.Generic;
using Base.View;
using Base.Extensions;
namespace Base
{
    public class BasicLevelLogic : Extendable<LevelLogicExtension>
    {
        private Square lastClickedSquare;
        private SimpleField<int> FIELD_Moves = new SimpleField<int>("Moves");

        protected void Awake() {
            RegisterAllExtensions();
            BasicGameLogic.INSTANCE.FIELD_CurrentLevel.Value = this;
        }

        public void CircleClick(Circle circle) {
            foreach (LevelLogicExtension extension in extensions) {
                if (extension.ConsumeCircleClick(circle))
                    return;
            }
            if (lastClickedSquare != null) {
                FitSquareInCircle(lastClickedSquare, circle);
                lastClickedSquare = null;
            }
        }

        protected void FitSquareInCircle(Square square, Circle circle) {
            if (square.Size <= circle.Size) {
                square.FitCircle = circle;
                FIELD_Moves.Value++;
            }
        }

        public void SquareClick(Square square) {
            foreach (LevelLogicExtension extension in extensions) {
                if (extension.ConsumeSquareClick(square))
                    return;
            }
            if (square.FitCircle == null)
                lastClickedSquare = square;
        }

        public void CollectPlayerStats(List<FieldBase> stats) {
            stats.Add(FIELD_Moves);
            foreach(LevelLogicExtension extension in extensions) {
                extension.CollectPlayerStats(stats);
            }
        }
    }
}
