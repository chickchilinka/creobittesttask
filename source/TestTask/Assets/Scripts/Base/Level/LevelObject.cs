﻿using System.Collections;
using UnityEngine;

namespace Base
{
    public class LevelObject : MonoBehaviour
    {
        private BasicLevelLogic levelLogic;
        protected BasicLevelLogic LevelLogic {
            get{
                if (levelLogic == null)
                    levelLogic = BasicGameLogic.INSTANCE.FIELD_CurrentLevel.Value;
                return levelLogic;
            }
        }
        
        private RectTransform _rectTransform;
        public RectTransform rectTransform {
            get {
                if (_rectTransform == null)
                    _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }

    }
}