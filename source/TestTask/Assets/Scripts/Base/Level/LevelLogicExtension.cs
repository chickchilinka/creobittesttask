﻿using UnityEngine;
using System.Collections.Generic;
using Base.View;
namespace Base.Extensions
{
    [RequireComponent(typeof(BasicLevelLogic))]
    public abstract class LevelLogicExtension : MonoBehaviour, IExtension
    {
        private BasicLevelLogic baseLogic;
        protected BasicLevelLogic BaseLogic {
            get{
                if (baseLogic == null)
                    baseLogic = BasicGameLogic.INSTANCE.FIELD_CurrentLevel.Value;
                return baseLogic;
            }
        }
        public abstract bool ConsumeCircleClick(Circle circle);
        public abstract bool ConsumeSquareClick(Square circle);
        public abstract void CollectPlayerStats(List<FieldBase> stats);
        public abstract string GetName();
    }
}
