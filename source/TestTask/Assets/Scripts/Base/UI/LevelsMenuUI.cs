﻿using UnityEngine;
using UnityEngine.UI;
using Base.Data;

namespace Base.UI
{
    public class LevelsMenuUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject buttonsContainer;
        [SerializeField]
        private GameObject buttonPrefab;
        private LevelManager levelManager;
        [SerializeField]
        private float horizontalIndent = 40f;
        private RectTransform _rectTransform;
        public RectTransform rectTransform {
            get {
                if (_rectTransform == null)
                    _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }

        protected void Start() {
            levelManager = BasicGameLogic.INSTANCE.GetLevelManager();
            BasicGameLogic.INSTANCE.FIELD_CurrentLevel.AddMonitor(() => gameObject.SetActive(false));
            CreateButtonsForLevels(levelManager.GetLevelsData());
        }

        public void CreateButtonsForLevels(LevelsData data) {
            RectTransform container = buttonsContainer.GetComponent<RectTransform>();
            float position = 0f-horizontalIndent*data.Levels.Count/2;
            foreach(Level level in data.Levels) {
                CreateButton(level, position, container);
                position += horizontalIndent;
            }
        }
        public void CreateButton(Level level, float xPos, RectTransform container) {
            GameObject button = Instantiate(buttonPrefab, container);
            button.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos, 0);
            button.GetComponent<Text>().text = level.Name;
            button.GetComponent<Button>().onClick.AddListener(()=>BasicGameLogic.INSTANCE.SetLevel(level));
        }
    }
}
