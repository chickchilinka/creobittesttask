﻿using System;
using UnityEngine;
namespace Base
{
    [Serializable]
    public class Level
    {
        [SerializeField]
        private string name;
        [SerializeField]
        private string sceneName;

        public string Name { get => name; }
        public string SceneName { get => sceneName; }

        public Level(string name, string sceneName) {
            this.name = name;
            this.sceneName = sceneName;
        }
    }
}
