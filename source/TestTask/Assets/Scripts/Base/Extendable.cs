﻿using UnityEngine;
using System.Collections.Generic;
namespace Base.Extensions
{
    public class Extendable<T> : MonoBehaviour where T : MonoBehaviour, IExtension 
    {
        protected List<T> extensions = new List<T>();

        public void RegisterAllExtensions() {
            foreach(T extension in GetComponents<T>()) {
                RegisterExtension(extension);
            }
        }

        public void RegisterExtension(T extension) {
            if(!extensions.Contains(extension))
                extensions.Add(extension);
        }

        public D GetExtension<D>() where D : MonoBehaviour, T {
            foreach (T logicExtension in extensions) {
                if (logicExtension.GetType() == typeof(D)) {
                    return (D)logicExtension;
                }
            }
            return null;
        }
    }
}