﻿using UnityEngine;
namespace Base.Extensions
{
    [RequireComponent(typeof(BasicGameLogic))]
    public abstract class GameLogicExtension : MonoBehaviour, IExtension
    {
        public abstract string GetName();
    }
}
