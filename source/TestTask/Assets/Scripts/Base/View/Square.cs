﻿using UnityEngine;
using UnityEngine.EventSystems;
namespace Base.View
{
    public class Square : SizedLevelObject, IPointerClickHandler
    {
        private static float MOVE_SPEED = 3000f;
        private Circle fitCircle;
        public Circle FitCircle {
            get => fitCircle;
            set {
                fitCircle = value;
                movePosition = fitCircle.rectTransform.anchoredPosition;
                move = true;
            }
        }
        private Vector2 movePosition;
        private bool move;

        public void OnPointerClick(PointerEventData eventData) {
            LevelLogic.SquareClick(this);
        }

        protected override float GetRealSize(int size) {
            return size;
        }

        protected void Update() {
            if (move) {
                rectTransform.anchoredPosition = Vector2.MoveTowards(this.rectTransform.anchoredPosition, movePosition, Time.deltaTime * MOVE_SPEED);
                move = Vector2.Distance(rectTransform.anchoredPosition, movePosition) > 0.01f;
            }
        }
    }
}
