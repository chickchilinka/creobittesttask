﻿using UnityEngine;
using UnityEngine.EventSystems;
namespace Base.View
{
    public class Circle : SizedLevelObject, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData) {
            LevelLogic.CircleClick(this);
        }

        protected override float GetRealSize(int size) {
            return Mathf.Sqrt(2) * size;
        }
    }
}
