﻿using UnityEngine;
using Base.Extensions;
using Base.View;
using Base;
using Extensions.Triangles.View;
using System.Collections.Generic;

namespace Extensions.Triangles
{
    public class TriangleLevelExtension : LevelLogicExtension
    {
        private Triangle lastClickedTriangle;
        private SimpleField<int> FIELD_Energy = new SimpleField<int>("Energy");
        [SerializeField]
        private int startEnergy;

        protected void Awake() {
            FIELD_Energy.Value = startEnergy;
        }
        public void TriangleClicked(Triangle triangle) {
            if (lastClickedTriangle != triangle) {
                lastClickedTriangle = triangle;
            }
            else {
                lastClickedTriangle = null;
            }
        }
        public override bool ConsumeCircleClick(Circle circle) {
            return false;
        }
        public override bool ConsumeSquareClick(Square square) {
            if (lastClickedTriangle != null && FIELD_Energy.Value > 0) {
                lastClickedTriangle.gameObject.SetActive(false);
                lastClickedTriangle = null;
                square.Size -= 1;
                FIELD_Energy.Value--;
                return true;
            }
            return false;
        }
        public override string GetName() {
            return "Triangle";
        }

        public override void CollectPlayerStats(List<FieldBase> stats) {
            stats.Add(FIELD_Energy);
        }
    }
}
