﻿using UnityEngine;
namespace Base.View
{
    public abstract class SizedLevelObject : LevelObject
    {
        private static float PIXEL_SIZE_MULTIPLIER = 20f;
        [SerializeField]
        protected int _size;
        public int Size {
            get => _size;
            set {
                if (value >= 1) {
                    _size = value;
                    UpdateSize(_size);
                }
            }
        }

        protected void OnValidate() {
            UpdateSize(_size);
        }

        public void UpdateSize(int size) {
            float realSize = GetRealSize(size);
            rectTransform.sizeDelta = new Vector2(realSize * PIXEL_SIZE_MULTIPLIER, realSize * PIXEL_SIZE_MULTIPLIER);
        }
        protected abstract float GetRealSize(int size);
    }
}