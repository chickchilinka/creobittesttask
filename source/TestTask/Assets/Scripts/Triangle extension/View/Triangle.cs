﻿using UnityEngine.EventSystems;
using Base;
namespace Extensions.Triangles.View
{
    public class Triangle : LevelObject, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData) {
            LevelLogic.GetExtension<TriangleLevelExtension>().TriangleClicked(this);
        }
    }
}
