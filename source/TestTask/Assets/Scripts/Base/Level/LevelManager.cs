﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Base.Data;

namespace Base
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField]
        private LevelsData levelsData;
        public delegate void OnLevelLoadDone();
        public delegate void OnLevelUnloadDone();
        private Level lastLevel;
        
        public void LoadDefaultLevel(OnLevelLoadDone onDone) {
            SetLevel(levelsData.GetDefault(), onDone);
        }
        
        public LevelsData GetLevelsData() {
            return levelsData;
        }

        public void SetLevel(Level level, OnLevelLoadDone onDone) {
            if (lastLevel != null) {
                UnloadLevel(lastLevel, () => {
                    LoadLevel(level, onDone);
                });
            }
            else {
                LoadLevel(level, onDone);
            }
        }

        public void UnloadLevel(Level level, OnLevelUnloadDone onDone) {
            AsyncOperation sceneUnload = SceneManager.UnloadSceneAsync(level.SceneName);
            sceneUnload.completed += (op) => {
                onDone();
                lastLevel = null;
            };
        }

        public void LoadLevel(Level level, OnLevelLoadDone onDone) {
            int lastLevelIndex = SceneManager.sceneCount;
            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(level.SceneName, LoadSceneMode.Additive);
            sceneLoad.completed += (op) => {
                onDone();
                lastLevel = level;
            };
        }
    }
}