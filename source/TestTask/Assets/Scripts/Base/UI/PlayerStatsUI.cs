﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Base.UI {
    public class PlayerStatsUI : MonoBehaviour
    {
        [SerializeField]
        protected Text statsText;
        [SerializeField]
        protected float defaultHeight = 50f;
        [SerializeField]
        protected float textPadding = 5f;
        protected List<FieldBase> stats;
        private RectTransform _rectTransform;
        public RectTransform rectTransform {
            get {
                if (_rectTransform == null)
                    _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }

        protected void Start() {
            BasicGameLogic.INSTANCE.FIELD_CurrentLevel.AddMonitorAndCall(() => {
                stats = BasicGameLogic.INSTANCE.CollectPlayerStats();
                foreach (FieldBase stat in stats) {
                    stat.AddMonitor(UpdateText);
                }
                UpdateText();
            });
        }

        protected void UpdateText() {
            string text = "";
            foreach(FieldBase stat in stats) {
                text += stat.ToString()+" ";
            }
            statsText.text = text;
            rectTransform.sizeDelta = new Vector2(statsText.preferredWidth + textPadding * 2, defaultHeight);
        }
    }
}
