﻿using System.Collections.Generic;
using Base.Extensions;
namespace Base
{
    public class BasicGameLogic : Extendable<GameLogicExtension>
    {
        public readonly SimpleField<BasicLevelLogic> FIELD_CurrentLevel = new SimpleField<BasicLevelLogic>();
        private static BasicGameLogic instance;
        private LevelManager levelManager;

        public static BasicGameLogic INSTANCE {
            get => instance;
        }

        protected void Awake() {
            instance = this;
            levelManager = GetComponent<LevelManager>();
            levelManager.LoadDefaultLevel(LevelChanged);
        }

        public LevelManager GetLevelManager() {
            return levelManager;
        }

        private void LevelChanged() {

        }

        public void SetLevel(Level level) {
            levelManager.SetLevel(level, LevelChanged);
        }

        public List<FieldBase> CollectPlayerStats() {
            List<FieldBase> playerStats = new List<FieldBase>();
            FIELD_CurrentLevel.Value.CollectPlayerStats(playerStats);
            return playerStats;
        }
    }
}
