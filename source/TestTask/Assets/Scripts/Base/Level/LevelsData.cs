﻿using System.Collections.Generic;
using UnityEngine;

namespace Base.Data
{
    [CreateAssetMenu(fileName = "Levels Data", menuName = "Data/Levels", order = 1)]
    public class LevelsData:ScriptableObject
    {
        [SerializeField]
        private List<Level> levels;
        public List<Level> Levels {
            get => levels;
        }
        [SerializeField]
        private string defaultLevel;

        public Level GetDefault() {
            return GetLevel(defaultLevel);
        }

        public Level GetLevel(string name) {
            foreach (Level level in levels)
                if (level.Name == name)
                    return level;
            if(name == defaultLevel)
                return null;
            return GetDefault();
        }
    }
}
