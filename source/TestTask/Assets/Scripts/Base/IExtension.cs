﻿namespace Base.Extensions
{
    public interface IExtension
    {
        string GetName();
    }
}
